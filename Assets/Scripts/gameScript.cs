﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameScript : MonoBehaviour {

    public GameObject[] meatballs;

    public GameObject[] pterodactyls;
    pterodactylScript[] pteroScripts;
    public GameObject[] pteroSpawnPoints;

    public GameObject theLauncher;
    public Vector2 shadowVelocity;
    public Vector2 shadowLaunchPosition;
    public int currentTrajectoryPath = 0;
    public GameObject trajectoryDot;
    GameObject[] trajectoryDots;
    trajectoryDotScript[] trajectoryDotScripts;

    SpringJoint2D launcherSpring;
    bool trajectoryCalculationEnabled = false;

    // Data used 
    int currentLevel = 1;
    int currentAvailableMeatballs;
    int currentPterodactylToSpawn;
    int currentPterodactylHunger;
    int currentMaxConcurrentPterodactyl;
    int currentBossPterodactyl;
    int lastLevelCarryOverMeatballs;

    int levelStartCountdown = 210;
    bool levelStarted = false;
    int lives = 3;
    bool allFed = false;
    bool gameRunning = true;
    int pteroSpawnInterval = 180;

    // Use this for initialization
    void Start () {
        launcherSpring = theLauncher.GetComponent<SpringJoint2D>();
        trajectoryDots = new GameObject[15];
        trajectoryDotScripts = new trajectoryDotScript[15];
        for (int x = 0; x < 15; x++) {
            Vector2 dotPosition = new Vector2(0f, -6f);
            GameObject trajectoryDotInstance = Instantiate(trajectoryDot);
            trajectoryDotInstance.transform.position = dotPosition;
            trajectoryDotScripts[x] = trajectoryDotInstance.GetComponent<trajectoryDotScript>();
            trajectoryDots[x] = trajectoryDotInstance;
        }
        pteroScripts = new pterodactylScript[5];
        for (int x = 0; x < 5; x++) {
            pteroScripts[x] = pterodactyls[x].GetComponent<pterodactylScript>();
        }
    }
	
	// Update is called 60 times per second
	void FixedUpdate () {
        if (gameRunning) {
            if (trajectoryCalculationEnabled)
                StartCoroutine(CalculateAndShowTrajectory());

            if (!levelStarted)
                if (levelStartCountdown > 0)
                    levelStartCountdown--;
                else {
                    setCurrentLevelData();
                    levelStartCountdown = 210;
                    levelStarted = true;
                } 
            else {
                if (lives == 0 || allFed)
                    PauseGame();
                if (ShouldAddPterodactyl()) {
                    CallInPterodactyl();
                }
            }
        }
    }

    bool ShouldAddPterodactyl () {
        if (pteroSpawnInterval > 0) {
            pteroSpawnInterval--;
            return false;
        }

        int currentAmountHunting = 0;
        foreach (var pteroScript in pteroScripts) {
            if (pteroScript.isHunting)
                currentAmountHunting++;
        }
        if (currentAmountHunting < currentMaxConcurrentPterodactyl) {
            pteroSpawnInterval = Random.Range(120, 180);
            return true;
        }

        return false;
    }

    void CallInPterodactyl () {
        currentPterodactylToSpawn--;
        foreach (var pteroScript in pteroScripts) {
            if (!pteroScript.isHunting) {
                int pos = Random.Range(0, 2);
                pteroScript.startHunting(pteroSpawnPoints[pos].transform.position);
                return;
            }
        }
    }

    public IEnumerator loadUp() {
        yield return new WaitForSeconds(0.3f);
        foreach (var meatball in meatballs) {
            if(!meatball.activeInHierarchy) {
                meatball.transform.position = theLauncher.transform.position;
                meatball.SetActive(true);
                launcherSpring.connectedBody = meatball.GetComponent<Rigidbody2D>();
                launcherSpring.enabled = true;
                yield break;
            }
        }
    }

    public void SignalTrajectoryCalculation (bool isEnabled) {
        if (isEnabled)
            trajectoryCalculationEnabled = true;
        else {
            trajectoryCalculationEnabled = false;
            for (int x = 0; x < 15; x++)
                trajectoryDotScripts[x].setEnabled(false);
        }
    }

    IEnumerator CalculateAndShowTrajectory () {
        int trajectoryPathAtCall = currentTrajectoryPath;
        yield return new WaitForSeconds(0.1f);
        for (int x = 0; x < 15; x++)
            if (trajectoryPathAtCall == currentTrajectoryPath && trajectoryCalculationEnabled) {
                trajectoryDots[x].transform.position = CalculateTrajectoryPosition(0.1f * x);
                if (!trajectoryDotScripts[x].trajectoryEnabled)
                    trajectoryDotScripts[x].setEnabled(true);
                yield return new WaitForSeconds(0.1f);
            } else
                x = 15;
    }

    Vector2 CalculateTrajectoryPosition (float timeStep) {
        return Physics2D.gravity * timeStep * timeStep * 0.5f + shadowVelocity * timeStep + shadowLaunchPosition;
    }

    void PauseGame () {
        gameRunning = false;
        if (lives == 0) {
            currentLevel = 1;
        } else {
            currentLevel++;
        }
        CalculateLevelScore();
        levelStarted = false;
    }

    void CalculateLevelScore () {
        Debug.Log(currentLevel);
    }

    void setCurrentLevelData () {
        currentAvailableMeatballs = Levels.availableMeatballs[currentLevel];
        currentPterodactylToSpawn = Levels.pterodactylToSpawn[currentLevel];
        currentPterodactylHunger = Levels.pterodactylHunger[currentLevel];
        currentMaxConcurrentPterodactyl = Levels.maxConcurrentPterodactyl[currentLevel];
        currentBossPterodactyl = Levels.bossPterodactyl[currentLevel];
    }
}


/*  TODO
 *  Make trajectory projection show up faster.
 *  Make pterodactyl fly closer towards a certain screen point.
 *  Reactivate feeding for pterodactyl after first fed complete.
 *  Figure out how to make them "touch" the finish line.
 * 
 * 
 * 
 * 
 * 
 */
