﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meatballShadowScript : MonoBehaviour {

    public SpringJoint2D shadowLauncher;
    public Rigidbody2D myBody;
    public SpriteRenderer mySprite;
    public TrailRenderer theTrail;
    public gameScript theGameScript;
    public BoxCollider2D[] shadowWalls;

    public Vector2 shadowVelocity;
    public Vector2 mousePosition;

    float releaseDelay;
    public bool isPressed = false;
    Vector2 moveForce;
    int waitBetweenTrajectoryLaunches = 12;
    bool launching = false;

    void Start () {
        releaseDelay = 1 / (shadowLauncher.frequency * 5);
    }

    void FixedUpdate() {
        if (isPressed && !launching) {
            shadowLauncher.enabled = false;
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            myBody.velocity = Vector2.zero;
            myBody.angularVelocity = 0;
            myBody.gravityScale = 0;
            
            moveForce.x = (mousePosition.x + 10 - myBody.position.x) * 500;
            moveForce.y = (mousePosition.y - myBody.position.y) * 500;
            myBody.AddForce(moveForce);
            if (waitBetweenTrajectoryLaunches == 0) {
                launching = true;
                StartCoroutine(calculateShadowVelocity());
                waitBetweenTrajectoryLaunches = 30;
            } else
                waitBetweenTrajectoryLaunches--;
        } else if(!isPressed) {
            myBody.velocity = Vector2.zero;
            myBody.angularVelocity = 0;
            myBody.gravityScale = 0;
        }
    }

    IEnumerator calculateShadowVelocity () {
        for (int x = 0; x < shadowWalls.Length; x++)
            shadowWalls[x].enabled = false;
        myBody.velocity = Vector2.zero;
        myBody.angularVelocity = 0;
        myBody.gravityScale = 1;
        shadowLauncher.enabled = true;

        yield return new WaitForSeconds(releaseDelay);
        shadowLauncher.enabled = false;
        theGameScript.shadowVelocity = new Vector2(myBody.velocity.x, myBody.velocity.y);
        theGameScript.shadowLaunchPosition = new Vector2(myBody.position.x - 10, myBody.position.y);
        for (int x = 0; x < shadowWalls.Length; x++)
            shadowWalls[x].enabled = true;
        launching = false;
    }

}
