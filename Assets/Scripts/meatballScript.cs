﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meatballScript : MonoBehaviour {

    public int id;
    public SpringJoint2D theLauncher;
    public Rigidbody2D myBody;
    public CircleCollider2D myCollider;
    public BoxCollider2D[] launcherWalls;
    public SpriteRenderer mySprite;
    public TrailRenderer theTrail;
    public gameScript theGameScript;
    public GameObject trajectoryDot;
    public GameObject shadowBall;

    bool isPressed = false;
    Vector2 mousePosition;
    Vector2 oldMousePosition;
    Vector2 moveForce;
    float releaseDelay;
    bool isFlying = false;
    meatballShadowScript shadowScript;

    void Start () {
        releaseDelay = 1 / (theLauncher.frequency * 5);
        oldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        shadowScript = shadowBall.GetComponent<meatballShadowScript>();
    }

    void FixedUpdate() {
        if (isPressed) {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            myBody.velocity = Vector2.zero;
            myBody.angularVelocity = 0;
            moveForce.x = (mousePosition.x - myBody.position.x) * 500;
            moveForce.y = (mousePosition.y - myBody.position.y) * 500;
            myBody.AddForce(moveForce);
            if (oldMousePosition != mousePosition) {
                oldMousePosition = mousePosition;
                theGameScript.currentTrajectoryPath++;
            }
            
        }
    }

    void OnMouseDown() {
        if (isFlying)
            return;
        myBody.gravityScale = 0;
        theLauncher.enabled = false;
        this.isPressed = true;
        shadowScript.transform.SetPositionAndRotation(new Vector2(myBody.position.x + 10, myBody.position.y), new Quaternion());
        shadowScript.isPressed = true;
        theGameScript.SignalTrajectoryCalculation(true);
        for (int x = 0; x < launcherWalls.Length; x++)
            launcherWalls[x].enabled = true;
    }

    void OnMouseUp () {
        if (isFlying)
            return;
        myBody.velocity = Vector2.zero;
        myBody.angularVelocity = 0;
        myBody.gravityScale = 1;
        theLauncher.enabled = true;
        this.isPressed = false;
        theGameScript.SignalTrajectoryCalculation(false);
        shadowScript.isPressed = false;
        for (int x = 0; x < launcherWalls.Length; x++)
            launcherWalls[x].enabled = false;

        StartCoroutine(Release());
    }

    IEnumerator Release() {
        yield return new WaitForSeconds(releaseDelay);
        theLauncher.enabled = false;
        isFlying = true;
        StartCoroutine(theGameScript.loadUp());
    }

    void OnCollisionEnter2D (Collision2D col) {
        if (col.gameObject.tag == "floor") {
            isFlying = false;
            mySprite.enabled = true;
            theTrail.enabled = false;
            this.gameObject.SetActive(false);
        }
    }

}
