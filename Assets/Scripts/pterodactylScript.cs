﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pterodactylScript : MonoBehaviour {

    public bool isHunting = false;
    public bool onScreen = false;
    public int feedAmount = 1;
    public float descentSpeed = 0.01f;

    bool pickupViking = false;

	// Use this for initialization
	void Start () {
		
	}

    void FixedUpdate () {
        if (isHunting) {
            this.transform.position = new Vector2(
                this.transform.position.x + calculateRandomHorizontalWobble(), 
                this.transform.position.y - descentSpeed);
        }
        if (pickupViking) {
            // TODO: Pickup viking fast and fly away.
        }
    }

    public void hitRegistered (bool headOrBody) {
        if (headOrBody) {
            feedAmount--;
            if (feedAmount == 0)
                stopHunting();
        } else {
            playAnnoyedAndIncreaseDescentSpeed();
        }
    }

    public void eatViking () {
        pickupViking = true;
        stopHunting();
    }

    public void startHunting (Vector2 startPosition) {
        this.transform.position = startPosition;
        isHunting = true;
    }

    void playAnnoyedAndIncreaseDescentSpeed () {

    }

    void stopHunting () {
        isHunting = false;
    }

    float calculateRandomHorizontalWobble () {
        float returnValue = 0f;

        if (this.transform.position.x > 2.0f) {
            returnValue = -0.2f;
        } else if (this.transform.position.x < -2.0f) {
            returnValue = 0.2f;
        } else {
            returnValue = Random.Range(-0.2f, 0.2f);
        }

        return returnValue;
    }

}
