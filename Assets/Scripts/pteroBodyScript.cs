﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pteroBodyScript : MonoBehaviour
{
    public pterodactylScript myPtero;

    void OnTriggerEnter2D (Collider2D col) {
        switch (col.gameObject.tag) {
            case "Meatball":
                myPtero.hitRegistered(false);
                break;
            case "finishLine":
                myPtero.eatViking();
                break;
            default:
                break;
        }
    }
}
