﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Levels
{
    public static int[] availableMeatballs = {
        10, 12, 12, 12, 15, 8, 20, 24, 30, 18,
        18, 26, 38, 45, 26, 36, 42, 56, 66, 45,
        54, 70, 80, 100, 70, 76, 108, 120, 140, 114
    };

    public static int[] pterodactylToSpawn = {
        3, 4, 5, 7, 0, 5, 7, 9, 12, 2,
        7, 10, 10, 12, 3, 10, 12, 14, 15, 5,
        12, 12, 14, 17, 8, 14, 15, 17, 20, 12
    };

    public static int[] pterodactylHunger = {
        1, 1, 1, 1, 0, 1, 2, 2, 2, 2,
        2, 2, 3, 3, 3, 3, 3, 4, 4, 4,
        4, 5, 5, 5, 5, 5, 6, 6, 6, 7
    };

    public static int[] pterodactylDescentSpeed = {
        // TODO
    };

    public static int[] maxConcurrentPterodactyl = {
        1, 1, 1, 1, 1, 1, 2, 2, 2, 2,
        2, 2, 2, 3, 3, 3, 3, 3, 3, 4,
        4, 4, 4, 4, 4, 5, 5, 5, 5, 5
    };

    public static int[] bossPterodactyl = {
        0, 0, 0, 0, 1, 0, 0, 0, 0, 2,
        0, 0, 0, 0, 3, 0, 0, 0, 0, 4,
        0, 0, 0, 0, 5, 0, 0, 0, 0, 6
    };

    public static int[] bossHunger = {
        8, 10, 12, 15, 20, 30
    };

    public static int[] bossFlightPattern = {
        // TODO
    };
}
