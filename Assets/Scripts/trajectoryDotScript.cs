﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trajectoryDotScript : MonoBehaviour
{
    SpriteRenderer myRenderer;
    Color spriteColor;
    bool fadingOut = true;
    public bool trajectoryEnabled = false;

    void Start() {
        myRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteColor = myRenderer.color;
    }

    void FixedUpdate() {
        if(trajectoryEnabled) 
            if(fadingOut) {
                spriteColor.a -= 0.02f;
                myRenderer.color = spriteColor;
                if (spriteColor.a <= 0)
                    fadingOut = false;
            } else {
                spriteColor.a += 0.02f;
                myRenderer.color = spriteColor;
                if (spriteColor.a >= 1)
                    fadingOut = true;
            }
    }

    public void setEnabled(bool isEnabled) {
        if (!isEnabled) {
            trajectoryEnabled = false;
            fadingOut = true;
            spriteColor.a = 0f;
            myRenderer.color = spriteColor;
        } else
            trajectoryEnabled = true;
    }
    
}
